package Regex_Example;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException
    {
        System.out.println( "Hello World!" );
        Regex r = new Regex();
        r.regex("^import.*");
        r.regex(".*\\bthrows\\b.*");
        r.regex(".* \\bclass\\b .*");
        //r.regex("/\\*([^*]|[\\r\\n])*\\*/\n" + "\n");

    }
}
