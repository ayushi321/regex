package Regex_Example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Regex {
   private boolean flag;
   private Pattern pattern;
   private Matcher matcher;

    Scanner sc = new Scanner(System.in);

    public void regex(String s) throws IOException {
        System.out.println("Enter path of the file");
        String path = sc.nextLine();

        File f = new File(path);
        FileReader fr = new FileReader(f);
        BufferedReader br = new BufferedReader(fr);

        String line;

        while ((line = br.readLine()) != null) {
            pattern = Pattern.compile(s);
            matcher = pattern.matcher(line);
            while (matcher.find()) {
                System.out.println(line);
            }
        }
    }
}




